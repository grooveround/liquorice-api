<?php

namespace App\Model;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int id
 * @property string title
 * @property string slug
 * @property string description
 * @property string html
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Status status
 * @property User user
 */
class Content extends Model
{
    use SoftDeletes, HasTimestamps;

    public $fillable = [
        'title',
        'slug',
        'description',
        'user_id',
        'status_id',
        'html'
    ];

    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
