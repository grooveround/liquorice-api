<?php

namespace App\Transformer;

use App\Model\Content;
use League\Fractal\TransformerAbstract;

/**
 * Class ContentTransformer
 * @package App\Transformer
 */
class ContentTransformer extends TransformerAbstract
{
    public $availableIncludes = ['user', 'status'];

    /**
     * @param Content $model
     * @return array
     */
    public function transform(Content $model)
    {
        return [
            'id' => $model->id,
            'title' => $model->title,
            'slug' => $model->slug,
            'description' => $model->description,
            'html' => $model->html,
            'status' => $model->status->name,
            'createdAt' => $model->created_at->toIso8601ZuluString(),
            'updatedAt' => $model->updated_at->toIso8601ZuluString()
        ];
    }

    /**
     * @param Content $model
     * @return \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
     */
    public function includeUser(Content $model)
    {
        if (!$model->user) {
            return $this->null();
        }

        return $this->item($model->user, new UserTransformer(), 'users');
    }

    /**
     * @param Content $model
     * @return \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
     */
    public function includeStatus(Content $model)
    {
        if (!$model->status) {
            return $this->null();
        }

        return $this->item($model->status, new StatusTransformer(), 'statuses');
    }
}