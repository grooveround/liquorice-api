<?php

namespace App\Transformer;

use App\User;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer
 * @package App\Transformer
 */
class UserTransformer extends TransformerAbstract
{
    public $availableIncludes = ['contents'];

    /**
     * @param User $model
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'email' => $model->email,
            'createdAt' => $model->created_at->toIso8601ZuluString(),
            'updatedAt' => $model->updated_at->toIso8601ZuluString()
        ];
    }

    /**
     * @param User $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeContents(User $model)
    {
        return $this->collection($model->contents, new ContentTransformer(), 'contents');
    }
}