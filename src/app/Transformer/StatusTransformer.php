<?php

namespace App\Transformer;

use App\Status;
use League\Fractal\TransformerAbstract;

/**
 * Class StatusTransformer
 * @package App\Transformer
 */
class StatusTransformer extends TransformerAbstract
{
    public $availableIncludes = ['contents', 'group'];

    /**
     * @param Status $model
     * @return array
     */
    public function transform(Status $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
        ];
    }

    /**
     * @param Status $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeContents(Status $model)
    {
        return $this->collection($model->contents, new ContentTransformer(), 'contents');
    }
}