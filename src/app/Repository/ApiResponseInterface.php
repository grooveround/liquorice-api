<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Collection;
use League\Fractal\Resource\Item as ResourceItem;

/**
 * Interface ApiResponseInterface
 * @package App\Repository
 */
interface ApiResponseInterface
{
    /**
     * @param int $id
     * @param Collection|null $filters
     * @return ResourceItem
     */
    public function item(int $id, Collection $filters = null): Model;

    /**
     * @param Collection|null $filters
     * @return AbstractPaginator
     */
    public function collection(Collection $filters = null): AbstractPaginator;

    /**
     * @param Collection $data
     * @return Model
     */
    public function add(Collection $data): Model;

    /**
     * @param int $id
     * @param Collection $data
     * @return Model
     */
    public function update(int $id, Collection $data): Model;

    /**
     * @param int $id
     * @return Model
     */
    public function delete(int $id): Model;
}