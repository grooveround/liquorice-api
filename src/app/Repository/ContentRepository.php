<?php

namespace App\Repository;

use App\Model\Content;
use App\Model\Status;
use App\Transformer\ContentTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\AbstractPaginator;

/**
 * Class ContentRepository
 * @package App\Repository
 */
class ContentRepository implements ApiResponseInterface
{
    /**
     * @var Content
     */
    private $model;
    /**
     * @var ContentTransformer
     */
    private $transformer;

    public function __construct(Content $model, ContentTransformer $transformer)
    {
        $this->model = $model;
        $this->transformer = $transformer;
    }

    /**
     * @param int $id
     * @param Collection|null $filters
     * @return Model
     */
    public function item(int $id, Collection $filters = null): Model
    {
        return $this->model->find($id);
    }

    /**
     * @param Collection|null $filters
     * @return AbstractPaginator
     */
    public function collection(Collection $filters = null): AbstractPaginator
    {
        $order = snake_case($filters->get('orderBy', $this->model->getKeyName()));
        $direction = $filters->get('direction', 'desc');
        $query = $this->model->newModelQuery();

        $criteria = $filters->filter(function ($item, $key) {
            return $item && in_array(snake_case($key), array_merge($this->model->getFillable(), [$this->model->getKeyName(), MODEL::CREATED_AT]));
        });

        if ($criteria->count() > 0) {
            $criteria->each(function ($item, $key) use (&$query) {
                if ($key === 'id') {
                    $query->where(snake_case($key), '=', $item);
                } elseif (snake_case($key) === MODEL::CREATED_AT) {
                    $query->whereDate(MODEL::CREATED_AT, $item);
                } elseif (is_array($item)) {
                    $query->whereIn(snake_case($key), $item);
                } else {
                    $query->where(snake_case($key), 'like', "%$item%");
                }
            });
        }

        return $query
            ->whereNull('deleted_at')
            ->orderBy($order, $direction)
            ->paginate();
    }

    /**
     * @param Collection $data
     * @return Model
     */
    public function add(Collection $data): Model
    {
        $slug = str_slug($data->get('title'));

        $query = $this->model->where('slug', $slug);

        if ($query->exists()) {
            $slug = $slug . '-' . time();
        }

        $data->put('slug', $slug);

        $statusId = Status::where('slug', strtolower($data->get('status', 'Active')))->value('id');
        $data->put('status_id', $statusId);

        $model = $this->model->create($data->only('title', 'slug', 'description', 'slug', 'html', 'user_id', 'status_id')->toArray());

        return $model;
    }

    /**
     * @param int $id
     * @param Collection $data
     * @return Model
     */
    public function update(int $id, Collection $data): Model
    {
        $slug = $data->get('slug', str_slug($data->get('title')));
        $model = $this->model->findOrFail($id);
        $query = $this->model->where('slug', $slug);

        if ($query->exists()) {
            $slug = $slug . '-' . time();
        }

        $data->put('slug', $slug);
        if ($data->has('status')) {
            $statusId = Status::where('slug', strtolower($data->get('status', 'Inactive')))->value('id');
            $data->put('status_id', $statusId);
        }

        $model->update($data->all());

        return $model;
    }

    /**
     * @param int $id
     * @return Model
     */
    public function delete(int $id): Model
    {
        $model = $this->model->findOrFail($id);

        $model->delete();

        return $model;
    }
}