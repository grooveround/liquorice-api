<?php

namespace App\Http\Controllers;

use App\Repository\ContentRepository;
use App\Transformer\ContentTransformer;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\JsonApiSerializer;

/**
 * Class ContentController
 * @package App\Http\Controllers
 */
class ContentController extends Controller
{
    /**
     * @var ContentRepository
     */
    protected $repository;
    /**
     * @var ContentTransformer
     */
    protected $transformer;
    /**
     * @var Manager
     */
    protected $formatter;
    /**
     * @var Request
     */
    protected $request;

    /**
     * ContentController constructor.
     * @param Request $request
     * @param Manager $formatter
     * @param ContentRepository $repository
     * @param ContentTransformer $transformer
     */
    public function __construct(Request $request, Manager $formatter, ContentRepository $repository, ContentTransformer $transformer)
    {
        $this->request = $request;
        $this->formatter = $formatter;
        $this->repository = $repository;
        $this->transformer = $transformer;

        if (request()->header('Content-Type', 'application/json') == 'application/vnd.api+json') {
            $baseUrl = env('APP_URL');
            $this->formatter->setSerializer(new JsonApiSerializer($baseUrl));
        }

        $this->formatter->parseIncludes(explode(',', $request->input('include')));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        /** @var LengthAwarePaginator|Paginator $collection */
        $collection = $this->repository->collection(collect($request->all()));
        $data = $collection->getCollection();
        $resource = new Collection($data, $this->transformer, 'contents');

        if (!empty($collection)) {
            $resource->setPaginator(new IlluminatePaginatorAdapter($collection));
        }

        return response(
            $this->formatter->createData($resource)->toArray(),
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function show(Request $request, int $id)
    {
        $item = $this->repository->item($id, collect($request->all()));
        $resource = new Item($item, $this->transformer, 'contents');
        return response(
            $this->formatter->createData($resource)->toArray(),
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @return array
     */
    public function create(Request $request)
    {
        $request->merge([
            'user_id' => $request->user()->id,
            'status' => 'Active'
        ]);

        $item = $this->repository->add(collect($request->all()));
        $resource = new Item($item, $this->transformer, 'contents');
        return response(
            $this->formatter->createData($resource)->toArray(),
            Response::HTTP_CREATED
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function store(Request $request, int $id)
    {
        $item = $this->repository->update($id, collect($request->all()));
        $resource = new Item($item, $this->transformer, 'contents');
        return response(
            $this->formatter->createData($resource)->toArray(),
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function delete(Request $request, int $id)
    {
        $item = $this->repository->delete($id);
        $resource = new Item($item, $this->transformer, 'contents');

        return response(
            $this->formatter->createData($resource)->toArray(),
            Response::HTTP_NO_CONTENT
        );
    }
}
