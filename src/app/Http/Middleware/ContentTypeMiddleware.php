<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

class ContentTypeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('Content-Type') !== 'application/vnd.api+json'){
            throw new UnsupportedMediaTypeHttpException('This API only supports Content-Type: application/vnd.api+json please refer to JSONAPI documentation');
        }

        return $next($request);
    }
}
