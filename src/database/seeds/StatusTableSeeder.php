<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contentStatusId = DB::table('groups')->where('name', 'content-status')->value('id');
        DB::table('statuses')->insert([
            [
                'name' => "Active",
                'slug' => "active",
                'group_id' => $contentStatusId
            ],
            [
                'name' => "Disabled",
                'slug' => "disabled",
                'group_id' => $contentStatusId
            ],
        ]);
    }
}
