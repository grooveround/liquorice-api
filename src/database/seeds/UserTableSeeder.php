<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminUserId = DB::table('users')->insertGetId([
            'name' => "admin",
            'email' => "derick.cornelius.fynn@gmail.com",
            'password' => bcrypt("pass"),
            'remember_token' => str_random(10),
            'created_at' => DB::raw('NOW()'),
            'updated_at' => DB::raw('NOW()'),
        ]);

        $userRoles = DB::table('roles')->get()->map(function ($role) use ($adminUserId) {
            return [
                'user_id' => $adminUserId,
                'role_id' => $role->id
            ];
        });

        if (!empty($userRoles)) {
            DB::table('role_users')->insert(...$userRoles);
        }
    }
}
