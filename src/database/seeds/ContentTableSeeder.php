<?php

use Illuminate\Database\Seeder;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = DB::table('users')->where('email', 'derick.cornelius.fynn@gmail.com')->value('id');
        $groupId = DB::table('groups')->where('name', 'content-status')->value('id');
        $statusId = DB::table('statuses')->where('group_id', $groupId)
            ->where('slug', 'active')->value('id');
        $articles = [];
        for ($i = 0; $i < 100; $i++) {
            $articles[] = [
                'user_id' => $userId,
                'status_id' => $statusId,
                'title' => 'Dummy Content' . $i,
                'slug' => 'dummy-content' . $i,
                'description' => 'This is a dummy content',
                'html' => '<p>This is a dummy content</p>',
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ];
        }
        DB::table('contents')->insert($articles);
    }
}
