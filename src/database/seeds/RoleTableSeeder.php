<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fromDb = DB::table('roles')->get()->pluck('name');

        $data = [
            [
                'name' => "super-admin",
                'description' => "Super Admin",
            ],
            [
                'name' => "admin",
                'description' => "Admin",
            ],
            [
                'name' => "author",
                'description' => "Author",
            ],
            [
                'name' => "editor",
                'description' => "Editor",
            ],
            [
                'name' => "normal",
                'description' => "Public",
            ]
        ];

        $inserts = [];
        foreach ($data as $role) {
            if (!in_array($role['name'], $fromDb->toArray())) {
                $inserts[] = $role;
            }
        }

        if (!empty($inserts)) {
            DB::table('roles')->insert(...$inserts);
        }
    }
}
