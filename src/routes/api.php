<?php

use App\Http\Controllers\ContentController;
use App\Http\Middleware\ContentTypeMiddleware;
use App\Http\Requests\CreateContentRequest;
use App\Http\Requests\DeleteContentRequest;
use App\Http\Requests\GetContentRequest;
use App\Http\Requests\ListContentRequest;
use App\Http\Requests\ShowContentRequest;
use App\Http\Requests\UpdateContentRequest;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api', ContentTypeMiddleware::class)->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api', ContentTypeMiddleware::class)->get('contents', function (ListContentRequest $request) {
    return app(ContentController::class)->index($request);
})->name('content/index');

Route::middleware('auth:api')->post('contents', function (CreateContentRequest $request) {
    return app(ContentController::class)->create($request);
})->name('content/index');

Route::middleware('auth:api')->get('contents/{id}', function (ShowContentRequest $request, int $id) {
    return app(ContentController::class)->show($request, $id);
})->name('content/show')
    ->where('id', '[0-9]+');

Route::middleware('auth:api')->put('contents/{id}', function (UpdateContentRequest $request, int $id) {
    return app(ContentController::class)->store($request, $id);
})->name('content/update')
    ->where('id', '[0-9]+');

Route::middleware('auth:api')->delete('contents/{id}', function (DeleteContentRequest $request, int $id) {
    return app(ContentController::class)->delete($request, $id);
})->name('content/delete')
    ->where('id', '[0-9]+');
