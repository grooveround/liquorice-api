### Provision App

```sh
docker-compose up --build
```


### DB migration

```sh
docker exec -it api_liquorice-php_1 bash -c "php artisan migrate && php artisan passport:install"
```
Copy the following keys generated from you previous command to a safe place, you will need them in order to configure oauth credentials

```
Personal access client created successfully.
Client ID: 1
Client Secret: q9nPzNCm5S4gODLqfyoYbdMk0fjNbzNo6zb49AB5

Password grant client created successfully.
Client ID: 2
Client Secret: 0EN3gqDJaenCIOsrv1in3483IwDLb8sW5mDddDVW

```
`Password grant client` will be used for OAUTH settings

### DB Seeding
Let's configure out startup data

```sh
docker exec -it api_liquorice-php_1 bash -c "php artisan db:seed"
```